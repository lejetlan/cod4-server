# Cod4

Dockerized cod4x server

### Setup
You need to supply your own game files from cod4
The following folders need to be created in libs:
main
zone
usermaps
mods

Once created copy over main and zone from your cod4 installation, and copy any mods/usermaps into their appropriate folders.

### Build
Run ./update to build a local docker image
Run ./update push to build a local docker image and push it to the registry