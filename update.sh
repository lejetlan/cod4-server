#!/bin/bash
VERSION=$(date +%Y%m%d-%H%M%S)
docker build -t registry.npf.dk/cod4 --no-cache .
docker tag registry.npf.dk/cod4 registry.npf.dk/cod4:$VERSION

if [ $1 = "push" ]; then
  docker push registry.npf.dk/cod4
  docker push registry.npf.dk/cod4:$VERSION
fi
